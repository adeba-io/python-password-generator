# Python Password Generator

Generates a string of human readable ASCii characters to be used as a password.  

Usage: `python ppwg.py [OPTION(S)] | [L33T STRING]`

- `-h` or `--help`: Print help information
- `-c` or `--count` `<integer>`: How long is the string

The omission of these options is the same as using all of them.
You can combine these options.

- `-l` or `--lowercase`: Use lowercase characters
- `-u` or `--uppercase`: Use uppercase characters
- `-n` or `--numbers`: Use numbers
- `-s` or `--special`: Use special characters
