import json
import random

def readFile():
  f = open('leet_map.txt', 'r')
  return json.loads(f.read())

def leetify(input):
  str = ''
  dict = readFile()

  for c in input:
    l = c.lower()
    if l in dict and random.randint(0, 3) != 2:
      c = random.choice(dict[l])
    
    str += c

  return str
