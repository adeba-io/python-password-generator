# Putting this all here for now
import sys, getopt

FORMAT_MSG = "Usage: {0} [-h] [-c COUNT] [-l] [-u] [-n] [-s] [L33T STRING]"

HELP_MSG = \
"""
Creates a string of random characters to be used as a password

Usage:
    {0} [OPTION(S)] [L33T STRING]

Options:
    [-h | --help]             : Prints the help information
    [-c | --count] <integer>  : Specify how long you want the string to be, 10 by default
    
  If you do not use any of the following options, all readable characters
  will be used to generate the string
    [-l | --lowercase]        : Use lowercase characters
    [-u | --uppercase]        : Use uppercase characters
    [-n | --numbers]          : Use numbers
    [-s | --special]          : Use special characters
"""

class Args:
  def __init__(self):
    self.length = 10
    self.lowercase = False
    self.uppercase = False
    self.numbers = False
    self.special = False
    self.leet = ''

  def use_all(self):
    return not (self.lowercase or self.uppercase or self.numbers or self.special)

def get_args():
  a = Args()

  for i in range(1, len(sys.argv)):
    ar = sys.argv[i]

    if ar.startswith('--'):
      if ar.startswith('count', 2):
        i += 1
        a.length = int(sys.argv[i])
      elif ar.startswith('lowercase', 2):  
        a.lowercase = True
      elif ar.startswith('uppercase', 2):
        a.uppercase = True
      elif ar.startswith('numbers', 2):
        a.numbers = True
      elif ar.startswith('special', 2):
        a.special = True
      elif ar.startswith('help', 2):
          print(HELP_MSG.format(sys.argv[0]))
          sys.exit(1)
      else:
        print(FORMAT_MSG.format(sys.argv[0]))
        sys.exit(1)

    elif ar[0] == '-':
      for j in range(1, len(ar)):
        if ar[j] == 'c':
          i += 1
          a.length = int(sys.argv[i])
        elif ar[j] == 'l':
          a.lowercase = True
        elif ar[j] == 'u':
          a.uppercase = True
        elif ar[j] == 'n':
          a.numbers = True
        elif ar[j] == 's':
          a.special = True
        elif ar[j] == 'h':
          print(HELP_MSG.format(sys.argv[0]))
          sys.exit(1)
        else:
          print(FORMAT_MSG.format(sys.argv[0]))
          sys.exit(1)
    elif i == 1 and ar:
      a.leet = ar
    else:
      print(FORMAT_MSG.format(sys.argv[0]))
      sys.exit(1)

  return a
