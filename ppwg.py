#!/usr/bin/python

from random import *
from leet import leetify
from args import *

RANGE = (33, 126) # Inclusive
LOWERCASE = (97, 122)
UPPERCASE = (65, 90)
NUMBERS = (48, 57)
SPECIAL = [(33, 47), (58, 64), (91, 96), (123, 126)]

def get_special():
    i = randint(0, len(SPECIAL) - 1)
    return SPECIAL[i]

char_switcher = {
    1: LOWERCASE,
    2: UPPERCASE,
    3: NUMBERS,
    4: get_special()
}

class Switcher:
    def __init__(self, args):
        self.dict = []

        if args.use_all():
            self.dict = [
                LOWERCASE,
                UPPERCASE,
                NUMBERS,
                get_special()
            ]
        else:
            if args.lowercase:
                self.dict.append(LOWERCASE)
            
            if args.uppercase:
                self.dict.append(UPPERCASE)
            
            if args.numbers:
                self.dict.append(NUMBERS)
            
            if args.special:
                self.dict.append(get_special)
        
        self.max = len(self.dict) - 1
    
    def get_char(self):
        r = randint(0, self.max)
        range = self.dict[r]
        if callable(range):
            range = range()

        return chr(randint(range[0], range[1]))

def randomString(args):
    switcher = Switcher(args)
    s = ''

    for i in range(args.length):
        s += switcher.get_char()
        # if args.useLimitedTypes():
        #     R = char_switcher.get(randint(1, 4), RANGE)
        #     s += chr(randint(R[0], R[1]))
        

    return s

def run(args):
    if args.leet:
        return leetify(args.leet)
    return randomString(args)

if __name__ == "__main__":
    a = get_args()
    print(run(a))
